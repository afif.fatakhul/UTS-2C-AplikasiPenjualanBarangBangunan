package afifudin.risfandi.myapplication

import android.content.ContentValues
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_barang.*

class BarangActivity :  AppCompatActivity(), View.OnClickListener {
    lateinit var db: SQLiteDatabase
    lateinit var lsadapter: ListAdapter
    var brgid: String = ""
    var barangId: String = ""
    var barangNama: String = ""
    var barangHarga: String = ""
    var barangSatuan: String = ""
    var barangStok: String = ""
    var kategoriId: String = ""

    val dtBrg: Int = 100
    fun getDbObject(): SQLiteDatabase {
        db = DBHelper(this).writableDatabase
        return db
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnAddBarang -> {
                var intent = Intent(this, AddBarangActivity::class.java)
                intent.putExtra("isPage", "Add")
                startActivityForResult(intent, dtBrg)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_barang)
        btnAddBarang.setOnClickListener(this)
        listBarang.setOnItemClickListener(listClick)
        var paket : Bundle? = intent.extras
        barangId = paket?.getString("barangId").toString()
        var del = paket?.getString("isDeleted").toString()
        if(del.equals("yes")){
            var snackbar = Snackbar.make(LinearLayout, "1 barang telah dihapus", Snackbar.LENGTH_LONG)
                .setAction("Undo", undoDelete)
            snackbar.show()
        }
        getDbObject()

        val actionbar = supportActionBar
        actionbar!!.title = "Data Barang"
//        Back Button
        actionbar.setDisplayHomeAsUpEnabled(true)
    }
    fun undoDelaBrang(){
        var dtNull = "0000-00-00 00:00:00"
        var cv : ContentValues = ContentValues()
        cv.put("tgl_deleted",dtNull)
        db.update("barang",cv,"barangId = $barangId",null)
    }
    val undoDelete = View.OnClickListener{
        undoDelaBrang()
        showDataBarang()
        var snackbar = Snackbar.make(LinearLayout,"Kategori berhasil di Undo", Snackbar.LENGTH_SHORT)
        snackbar.show()
    }

    fun showDataBarang() {
        val c: Cursor = db.rawQuery(
            "select brg.barangId as _id,brg.barangNama as brgNama, brg.barangHarga as brgHarga, brg.barangSatuan as brgSatuan, brg.barangStok as brgStok,ktg.kategoriNama as katNM, brg.tgl_deleted as brgDel from barang brg,kategori ktg where brg.kategoriId=ktg.kategoriId and brgDel = '0000-00-00 00:00:00'  order by brgNama asc",
            null
        )
        lsadapter = SimpleCursorAdapter(
            this, R.layout.item_barang, c,
            arrayOf("brgNama", "katNM"), intArrayOf(R.id.namaBrg, R.id.ktgNama),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        listBarang.adapter = lsadapter
    }

    val listClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        brgid = c.getString(c.getColumnIndex("_id"))
        barangNama = c.getString(c.getColumnIndex("brgNama"))
        barangHarga = c.getString(c.getColumnIndex("brgHarga"))
        barangSatuan = c.getString(c.getColumnIndex("brgSatuan"))
        barangStok = c.getString(c.getColumnIndex("brgStok"))
        kategoriId = c.getString(c.getColumnIndex("katNM"))
        var intent = Intent( this,AddBarangActivity::class.java)
        intent.putExtra("isPage","Edit")
        intent.putExtra("barangId",brgid)
        intent.putExtra("barangNama",barangNama)
        intent.putExtra("barangHarga",barangHarga)
        intent.putExtra("barangStok",barangStok)
        intent.putExtra("barangSatuan",barangSatuan)
        intent.putExtra("kategoriId",kategoriId)
        startActivityForResult(intent,dtBrg)
    }

    override fun onStart() {
        super.onStart()
        showDataBarang()
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}