package afifudin.risfandi.myapplication

import android.content.ContentValues
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_addcustomer.*
import java.text.SimpleDateFormat
import java.util.*

class AddCustomerActivity: AppCompatActivity()  {

    lateinit var db : SQLiteDatabase
    var customerID : String = ""
    var nama : String=""
    var telepon : String=""
    var alamat : String=""
    var page : String=""
    val dtCstmr : Int = 100

    fun getDbObject() : SQLiteDatabase {
        db = DBHelper(this).writableDatabase
        return db
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_activity,menu)
        var paket : Bundle? = intent.extras
        page = paket?.getString("isPage").toString()
        if(page.equals("Add")){
            menu?.findItem(R.id.btnDelete)?.setVisible(false)
        }else{
            menu?.findItem(R.id.btnDelete)?.setVisible(true)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addcustomer)
        val actionbar = supportActionBar
        var paket : Bundle? = intent.extras
        page = paket?.getString("isPage").toString()
        if(page.equals("Edit")){
            customerID = paket?.getString("customer_id").toString()
            txtNama.setText(paket?.getString("customer_nama"))
            txtTelp.setText(paket?.getString("customer_telp"))
            txtALamat.setText(paket?.getString("customer_almt"))
            actionbar!!.title = "Edit Customer"
        }
        actionbar!!.title = "Tambah Customer"
        getDbObject()
//        Back Button
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.btnSimpan -> {
                var paket : Bundle? = intent.extras
                page = paket?.getString("isPage").toString()
                if(page.equals("Add")){
                    addCustomer()
                    Toast.makeText(this,"Berhasil menngubah data customer", Toast.LENGTH_SHORT).show()
                    onBackPressed()
                }else{
                    updateCustomer()
                    Toast.makeText(this, "Berhasil menambahkan data customer", Toast.LENGTH_SHORT).show()
                    onBackPressed()
                }
                return true
            }
            R.id.btnDelete -> {
                deleteCustomer()
                var intent = Intent(this, CustomerActivity::class.java)
                intent.putExtra("isDeleted", "yes")
                intent.putExtra("customer_id", customerID)
                startActivityForResult(intent, dtCstmr)
            }R.id.btnRefresh -> {
            clearData()
        }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    fun clearData(){
        txtNama.setText("")
        txtTelp.setText("")
        txtALamat.setText("")
    }

    fun addCustomer() {
        nama = txtNama.text.toString()
        telepon = txtTelp.text.toString()
        alamat = txtALamat.text.toString()
        var cv : ContentValues = ContentValues()
        cv.put("customerNm", nama)
        cv.put("customerTelp",telepon)
        cv.put("customerAlmt",alamat)
        db.insert("customer",null,cv)
    }

    fun getDate(): String? {
        var date =  SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(System.currentTimeMillis())
        return date
    }

    fun updateCustomer() {
        nama = txtNama.text.toString()
        telepon = txtTelp.text.toString()
        alamat = txtALamat.text.toString()
        var cv : ContentValues = ContentValues()
        cv.put("customerNm", nama)
        cv.put("customerTelp",telepon)
        cv.put("customerAlmt",alamat)
        db.update("customer",cv,"customerId = $customerID",null)
    }

    fun deleteCustomer(){
        var cv : ContentValues = ContentValues()
        cv.put("tgl_deleted",getDate())
        db.update("customer",cv,"customerId = $customerID",null)
    }



}