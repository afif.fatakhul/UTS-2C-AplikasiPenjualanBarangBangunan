package afifudin.risfandi.myapplication

import android.content.ContentValues
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_listtransaksi.*

class TransaksiActivity :  AppCompatActivity(), View.OnClickListener {
    lateinit var db: SQLiteDatabase
    lateinit var lsadapter: ListAdapter
    var trId : String = ""
    var trXid : String = ""
    var trBrg : String = ""
    var trhrgBrg : String = ""
    var jmlTr : String = ""
    var totalTr : String = ""
    var kembalianTr : String = ""
    var tgl : String = ""
    var cstmr : String = ""

    val dtTr: Int = 100
    fun getDbObject(): SQLiteDatabase {
        db = DBHelper(this).writableDatabase
        return db
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnAddTr -> {
                var intent = Intent(this, AddTransaksi::class.java)
                intent.putExtra("isPage", "Add")
                startActivityForResult(intent, dtTr)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listtransaksi)
        btnAddTr.setOnClickListener(this)
        listTr.setOnItemClickListener(listClick)
        getDbObject()
        var paket : Bundle? = intent.extras
        trXid = paket?.getString("trId").toString()
        var del = paket?.getString("isDeleted").toString()
        if(del.equals("yes")){
            var snackbar = Snackbar.make(LinearLayout, "1 transaksi telah dihapus", Snackbar.LENGTH_LONG)
                .setAction("Undo", undoDelete)
            snackbar.show()
        }
        var inpt : String = ""
        inpt = paket?.getString("isUpdated").toString()
        val actionbar = supportActionBar
        actionbar!!.title = "Data Transaksi"
//        Back Button
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    fun undoDeleteTr(){
        var dtNull = "0000-00-00 00:00:00"
        var cv : ContentValues = ContentValues()
        cv.put("tgl_deleted",dtNull)
        db.update("transaksi",cv,"trId = $trXid",null)
    }
    val undoDelete = View.OnClickListener{
        undoDeleteTr()
        showDataTransaksi()
        var snackbar = Snackbar.make(LinearLayout,"Transaksi berhasil di Undo", Snackbar.LENGTH_SHORT)
        snackbar.show()
    }

    val listClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        trId = c.getString(c.getColumnIndex("_id"))
        trBrg = c.getString(c.getColumnIndex("barangNama"))
        trhrgBrg  = c.getString(c.getColumnIndex("barangHarga"))
        jmlTr = c.getString(c.getColumnIndex("jumlahBrg"))
        totalTr = c.getString(c.getColumnIndex("totalByr"))
        kembalianTr = c.getString(c.getColumnIndex("kembalian"))
        tgl = c.getString(c.getColumnIndex("tgl_tr"))
        cstmr = c.getString(c.getColumnIndex("customerNm"))
        var intent = Intent( this,DetailTransaksi::class.java)
        intent.putExtra("isPage","Edit")
        intent.putExtra("trId",trId)
        intent.putExtra("trBrg",trBrg)
        intent.putExtra("trhrgBrg",trhrgBrg)
        intent.putExtra("jmlTr",jmlTr)
        intent.putExtra("totalTr",totalTr)
        intent.putExtra("kembalianTr",kembalianTr)
        intent.putExtra("tgl",tgl)
        intent.putExtra("customer",cstmr)
        startActivityForResult(intent,dtTr)
    }


    fun showDataTransaksi() {
        val c: Cursor = db.rawQuery(
            "select tr.trId as _id, tr.jumlahBrg, tr.hargaBrg, tr.totalByr, tr.kembalian, brg.barangNama, brg.barangHarga, cs.customerNm, tr.tgl_tr, tr.tgl_deleted from transaksi tr,barang brg,customer cs where tr.brg_id=brg.barangId and tr.customer_id = cs.customerId and tr.tgl_deleted = '0000-00-00 00:00:00' order by tr.tgl_tr desc",
            null
        )
        lsadapter = SimpleCursorAdapter(
            this, R.layout.item_transaksi, c,
            arrayOf("barangNama", "tgl_tr"), intArrayOf(R.id.trBrg, R.id.tglTr),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        listTr.adapter = lsadapter
    }

    override fun onStart() {
        super.onStart()
        showDataTransaksi()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}