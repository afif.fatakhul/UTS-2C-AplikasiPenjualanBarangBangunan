package afifudin.risfandi.myapplication

import android.content.ContentValues
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_addkategori.*
import java.text.SimpleDateFormat
import java.util.*

class AddKategoriActivity : AppCompatActivity() {
    lateinit var db : SQLiteDatabase
    var kategoriId : String = ""
    var nama : String=""

    var page : String=""
    val dtKat : Int = 100

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_activity,menu)
        var paket : Bundle? = intent.extras
        page = paket?.getString("isPage").toString()
        if(page.equals("Add")){
            menu?.findItem(R.id.btnDelete)?.setVisible(false)
        }else{
            menu?.findItem(R.id.btnDelete)?.setVisible(true)
        }
        return super.onCreateOptionsMenu(menu)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addkategori)
        var paket : Bundle? = intent.extras
        page = paket?.getString("isPage").toString()
        if(page.equals("Edit")){
            kategoriId = paket?.getString("KategoriId").toString()
            kategoriNama.setText(paket?.getString("kategoriNama"))
        }
        getDbObject()
        val actionbar = supportActionBar
        actionbar!!.title = "Tambah Kategori"
//        Back Button
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.btnSimpan -> {
                var paket : Bundle? = intent.extras
                page = paket?.getString("isPage").toString()
                if(page.equals("Add")){
                    addKategori()
                    Toast.makeText(this,"Berhasil menambahkan data kategori", Toast.LENGTH_SHORT).show()
                    onBackPressed()
                }else{
                    updatekategori()
                    Toast.makeText(this,"Berhasil menngubah data kategori", Toast.LENGTH_SHORT).show()
                    onBackPressed()
                }
                return true
            } R.id.btnDelete -> {
                deletekategori()
                var intent = Intent(this, CategoryActivity::class.java)
                intent.putExtra("isDeleted","yes")
                intent.putExtra("kategoriId",kategoriId)
                startActivityForResult(intent,dtKat)
            }
            R.id.btnRefresh -> {
                clearData()
            }

        }
        return super.onOptionsItemSelected(item)
    }

    fun clearData(){
        kategoriNama.setText("")
    }

    fun getDbObject() : SQLiteDatabase {
        db = DBHelper(this).writableDatabase
        return db
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    fun getDate(): String? {
        var date =  SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(System.currentTimeMillis())
        return date
    }

    fun addKategori() {
        nama = kategoriNama.text.toString()

        var cv : ContentValues = ContentValues()
        cv.put("kategoriNama", nama)
        db.insert("kategori",null,cv)
    }
    fun updatekategori() {
        nama = kategoriNama.text.toString()

        var cv : ContentValues = ContentValues()
        cv.put("kategoriNama", nama)
        db.update("kategori",cv,"kategoriId = $kategoriId",null)
    }
    fun deletekategori() {
        nama = kategoriNama.text.toString()

        var cv : ContentValues = ContentValues()
        cv.put("tgl_deleted",getDate())
        db.update("kategori",cv,"kategoriId = $kategoriId",null)
    }

}