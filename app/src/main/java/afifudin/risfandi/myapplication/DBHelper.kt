package afifudin.risfandi.myapplication

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(context: Context):SQLiteOpenHelper(context, DB_Name, null, DB_ver) {

    companion object {
        val DB_Name = "db_tokoBangunan"
        val DB_ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tCstmr = "create table customer(customerId integer primary key autoincrement, customerNm varchar(50) not null, customerTelp varchar(20) not null, customerAlmt Text not null, tgl_deleted timestamp default '0000-00-00 00:00:00')"
        val tKtgr = "create table kategori(kategoriId integer primary key autoincrement ,kategoriNama varchar(50) not null,  tgl_deleted timestamp default '0000-00-00 00:00:00')"
        val tBrg = "create table Barang(barangId integer primary key autoincrement, barangNama varchar(50) not null, barangHarga integer(20) not null, barangSatuan varchar(20) not null, barangStok integer not null,kategoriId integer not null,tgl_deleted timestamp default '0000-00-00 00:00:00')"
        val tTr = "create table transaksi(trId integer primary key autoincrement, brg_id integer not null, customer_id integer not null , jumlahBrg integer(20) not null, hargaBrg integer(20) not null, totalByr integer(20) not null, kembalian integer(20) not null, tgl_tr timestamp not null,  tgl_deleted timestamp default '0000-00-00 00:00:00')"
        val tUsr = "create table user(userid integer primary key autoincrement, username varchar(30) not null, password varchar(30) not null)"
        val inUsr =  "insert into user(username,password) values('tokosemenjaya','12345678')"
        db?.execSQL(tCstmr)
        db?.execSQL(tKtgr)
        db?.execSQL(tBrg)
        db?.execSQL(tTr)
        db?.execSQL(tUsr)
        db?.execSQL(inUsr)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }
}