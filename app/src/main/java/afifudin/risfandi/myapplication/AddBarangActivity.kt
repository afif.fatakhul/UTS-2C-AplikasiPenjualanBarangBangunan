package afifudin.risfandi.myapplication

import android.content.ContentValues
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_addbarang.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AddBarangActivity : AppCompatActivity() {
    lateinit var db : SQLiteDatabase
    lateinit var adapterSpin : ArrayAdapter<String>
    var barangId : String = ""
    var brgsatuan : String = ""
    var barangNama : String=""
    var barangHarga : String=""
    val arraySatuan = arrayOf("M³","Gram","Kg","Biji","Sak","Meter","Gulung","Lembar","M2","Dus")
    var barangStok : String=""
    var kategoriNama : String=""
    var arrKategori = ArrayList<String>()
    var arrKt = ArrayList<String>()
    lateinit var spAdapter : SimpleCursorAdapter

    var page : String=""
    val dtBrg : Int = 100

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_activity,menu)
        var paket : Bundle? = intent.extras
        page = paket?.getString("isPage").toString()
        if(page.equals("Add")){
            menu?.findItem(R.id.btnDelete)?.setVisible(false)
        }else{
            menu?.findItem(R.id.btnDelete)?.setVisible(true)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addbarang)
        adapterSpin = ArrayAdapter( this, android.R.layout.simple_list_item_1,arraySatuan)
        var paket : Bundle? = intent.extras
        var ktNama : String = ""
        ktNama = paket?.getString("kategoriId").toString()
        page = paket?.getString("isPage").toString()
        if(page.equals("Edit")){
            barangId = paket?.getString("barangId").toString()
            namaBrg.setText(paket?.getString("barangNama"))
            hargaBrg.setText(paket?.getString("barangHarga"))
            stokBrg.setText(paket?.getString("barangStok"))
        }

        getDbObject()
        satuanBrg.adapter = adapterSpin
        val actionbar = supportActionBar
        actionbar!!.title = "Tambah Barang"
//        Back Button
        actionbar.setDisplayHomeAsUpEnabled(true)

        satuanBrg.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(baseContext, "Tidak ada yang dipilih", Toast.LENGTH_SHORT).show()
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                brgsatuan = adapterSpin.getItem(position).toString()
            }
        }

        kategori.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(baseContext, "Tidak ada yang dipilih", Toast.LENGTH_SHORT).show()
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val c = spAdapter.getItem(position) as Cursor
                kategoriNama = c.getString(c.getColumnIndex("_id"))
            }
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.btnSimpan -> {
                var paket : Bundle? = intent.extras
                page = paket?.getString("isPage").toString()

                if(page.equals("Add")){
                    addBarang()
                    Toast.makeText(this,"Berhasil menambahkan data barang", Toast.LENGTH_SHORT).show()
                    onBackPressed()
                }else{
                    updateBarang()
                    Toast.makeText(this,"Berhasil mengubah data barang", Toast.LENGTH_SHORT).show()
                    onBackPressed()
                }
                return true
            }
            R.id.btnDelete -> {
                deleteBarang()
                var intent = Intent(this, BarangActivity::class.java)
                intent.putExtra("isDeleted","yes")
                intent.putExtra("barangId",barangId)
                startActivityForResult(intent,dtBrg)
            }R.id.btnRefresh -> {
            clearData()
        }


        }
        return super.onOptionsItemSelected(item)
    }
    fun clearData(){
        namaBrg.setText("")
        hargaBrg.setText("")
        stokBrg.setText("")
        satuanBrg.setSelection(0)
        kategori.setSelection(0)

    }

    fun showDataKategori(){
        val c : Cursor = db.rawQuery("select kategoriNama as _id, tgl_deleted from kategori where tgl_deleted = '0000-00-00 00:00:00'  order by kategoriNama asc",null)
        spAdapter = SimpleCursorAdapter(this, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        kategori.adapter = spAdapter
        kategori.setSelection(0)
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()){
            temp = c.getString(c.getColumnIndex("_id"))
            arrKt.add(temp)
            c.moveToNext()
        }
    }

    fun getDate(): String? {
        var date =  SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(System.currentTimeMillis())
        return date
    }
    fun deleteBarang(){
        var cv : ContentValues = ContentValues()
        cv.put("tgl_deleted",getDate())
        db.update("barang",cv,"barangId = $barangId",null)
    }

    override fun onStart() {
        super.onStart()
        showDataKategori()
        var paket : Bundle? = intent.extras
        kategori.setSelection(getIndex(kategori,paket?.getString("kategoriId").toString()))
        satuanBrg.setSelection(getIndexSatuan(satuanBrg,paket?.getString("barangSatuan").toString()))
    }

    fun getDbObject() : SQLiteDatabase {
        db = DBHelper(this).writableDatabase
        return db
    }

    fun getIndex(spinner: Spinner, myString: String) : Int {
        var a = spinner.count
        var b : String = ""
        for (i in 0 until a){
            b = arrKt.get(i)
            if(b.equals(myString,ignoreCase = true)){
                return i
            }
        }
        return 0
    }

    fun getIndexSatuan(spinner: Spinner, myString: String) : Int {
        var a = spinner.count
        var b : String = ""
        for (i in 0 until a){
            b = arraySatuan.get(i)
            if(b.equals(myString,ignoreCase = true)){
                return i
            }
        }
        return 0
    }

    fun addBarang() {
        var sql = "select kategoriId from kategori where kategoriNama='$kategoriNama'"
        var c : Cursor = db.rawQuery(sql,null)
        c.moveToFirst()
        barangNama = namaBrg.text.toString()
        barangHarga = hargaBrg.text.toString()
        barangStok = stokBrg.text.toString()
        insertBrg(barangNama,barangHarga,brgsatuan,barangStok,c.getString(c.getColumnIndex("kategoriId")))
    }

    fun insertBrg(nama : String,harga : String, satuan : String, stok : String, kategoriId : String){
        var cv : ContentValues = ContentValues()
        cv.put("barangNama", nama)
        cv.put("barangHarga",harga)
        cv.put("barangSatuan", satuan)
        cv.put("barangStok",stok)
        cv.put("kategoriId",kategoriId)
        db.insert("barang",null,cv)
    }

    fun updateBarang(){
        var sql = "select kategoriId from kategori where kategoriNama='$kategoriNama'"
        var c : Cursor = db.rawQuery(sql,null)
        c.moveToFirst()
        barangNama = namaBrg.text.toString()
        barangHarga = hargaBrg.text.toString()
        barangStok = stokBrg.text.toString()
        updtbrg(barangNama,barangHarga,brgsatuan,barangStok,c.getString(c.getColumnIndex("kategoriId")))
    }

    fun updtbrg(nama : String,harga : String, satuan : String, stok : String, kategoriId : String){
        var cv : ContentValues = ContentValues()
        cv.put("barangNama", nama)
        cv.put("barangHarga",harga)
        cv.put("barangSatuan", satuan)
        cv.put("barangStok",stok)
        cv.put("kategoriId",kategoriId)
        db.update("barang",cv,"barangId = $barangId",null)
    }



    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}