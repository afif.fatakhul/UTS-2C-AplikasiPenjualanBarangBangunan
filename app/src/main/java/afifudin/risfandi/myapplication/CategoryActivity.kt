package afifudin.risfandi.myapplication

import android.content.ContentValues
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_kategori.*


class CategoryActivity :  AppCompatActivity(), View.OnClickListener{
    lateinit var db : SQLiteDatabase
    lateinit var lsadapter: ListAdapter
    var katid : String = ""
    var kategoriId : String = ""
    var kategoriNama : String = ""

    val dtKat : Int = 100

    fun getDbObject() : SQLiteDatabase {
        db = DBHelper(this).writableDatabase
        return db
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnAddKategori -> {
                var intent = Intent(this, AddKategoriActivity::class.java)
                intent.putExtra("isPage","Add")
                startActivityForResult(intent,dtKat)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kategori)
        btnAddKategori.setOnClickListener(this)
        listKategori.setOnItemClickListener(listClick)
        var paket : Bundle? = intent.extras
        katid = paket?.getString("kategoriId").toString()
        var del = paket?.getString("isDeleted").toString()
        if(del.equals("yes")){
            var snackbar = Snackbar.make(linearlayout, "1 kategori telah dihapus", Snackbar.LENGTH_LONG)
                .setAction("Undo", undoDelete)
            snackbar.show()
        }
        getDbObject()

        val actionbar = supportActionBar
        actionbar!!.title = "Data Kategori"
//        Back Button
        actionbar.setDisplayHomeAsUpEnabled(true)
    }
    fun undoDelKategori(){
        var dtNull = "0000-00-00 00:00:00"
        var cv : ContentValues = ContentValues()
        cv.put("tgl_deleted",dtNull)
        db.update("kategori",cv,"kategoriId = $katid",null)
    }

    val undoDelete = View.OnClickListener{
        undoDelKategori()
        showDataKategori()
        var snackbar = Snackbar.make(linearlayout,"Kategori berhasil di Undo", Snackbar.LENGTH_SHORT)
        snackbar.show()
    }
    fun showDataKategori(){
        val c : Cursor = db.rawQuery("select kategoriId as _id,kategoriNama, tgl_deleted from kategori where tgl_deleted = '0000-00-00 00:00:00'  order by kategoriNama asc",null)
        lsadapter = SimpleCursorAdapter(this, R.layout.item_kategori,c,
            arrayOf("kategoriNama"), intArrayOf(R.id.namaKategori),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        listKategori.adapter = lsadapter
    }
    override fun onStart() {
        super.onStart()
        showDataKategori()
    }
    val listClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        kategoriId = c.getString(c.getColumnIndex("_id"))
        kategoriNama = c.getString(c.getColumnIndex("kategoriNama"))
        var intent = Intent( this,AddKategoriActivity::class.java)
        intent.putExtra("isPage","Edit")
        intent.putExtra("KategoriId",kategoriId)
        intent.putExtra("kategoriNama",kategoriNama)
        startActivityForResult(intent,dtKat)
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}