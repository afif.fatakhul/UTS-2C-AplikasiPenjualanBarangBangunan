package afifudin.risfandi.myapplication

import android.content.ContentValues
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_transaksi.*
import java.text.SimpleDateFormat
import java.util.*

class AddTransaksi : AppCompatActivity() {
    lateinit var db : SQLiteDatabase
    var transaksiId : String = ""
    var customerId : String = ""
    var customerNm : String = ""
    var barangId : String = ""
    var brgId : String = ""
    var transaksiHrgBrg : Int = 0
    var transaksiJumlah : Int = 0
    var transaksiTotal : Int = 0
    var truangAnda : Int = 0
    var transaksiKembalian : String = ""
    var transaksiTgl : String = ""
    var brgHarga : String = ""

    lateinit var spAdapter : SimpleCursorAdapter
    lateinit var spAdapter2 : SimpleCursorAdapter

    var page : String=""
    val dtTr : Int = 100

    fun getDbObject() : SQLiteDatabase {
        db = DBHelper(this).writableDatabase
        return db
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_activity,menu)
        var paket : Bundle? = intent.extras
        page = paket?.getString("isPage").toString()
        if(page.equals("Add")){
            menu?.findItem(R.id.btnDelete)?.setVisible(false)
        }else{
            menu?.findItem(R.id.btnDelete)?.setVisible(true)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaksi)
        spBrg.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(baseContext, "Tidak ada yang dipilih", Toast.LENGTH_SHORT).show()
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val x = spAdapter.getItem(position) as Cursor
                barangId = x.getString(x.getColumnIndex("_id"))
                var sql2 = "select * from barang where barangNama='$barangId'"
                var c : Cursor = db.rawQuery(sql2,null)
                c.moveToFirst()
                brgId = c.getString(c.getColumnIndex("barangId"))
                brgHarga = c.getString(c.getColumnIndex("barangHarga"))

                var sql = "select barangHarga from barang where barangNama='$barangId'"
                var d : Cursor = db.rawQuery(sql,null)
                d.moveToFirst()
                transaksiHrgBrg = d.getInt(d.getColumnIndex("barangHarga"))
            }
        }
        spCustomer.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(baseContext, "Tidak ada yang dipilih", Toast.LENGTH_SHORT).show()
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val a = spAdapter2.getItem(position) as Cursor
                customerId = a.getString(a.getColumnIndex("_id"))
                var sql2 = "select * from customer where customerNm='$customerId'"
                var x : Cursor = db.rawQuery(sql2,null)
                x.moveToFirst()
                customerNm = x.getString(x.getColumnIndex("customerNm"))
            }
        }
        getDbObject()
        val actionbar = supportActionBar
        actionbar!!.title = "Tambah Transaksi"
//        Back Button
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onStart() {
        super.onStart()
        showDataBarang()
        showDataCustomer()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.btnSimpan -> {
                addTransaksi()
                Toast.makeText(this, "Berhasil menambahkan data transaksi", Toast.LENGTH_SHORT).show()
                onBackPressed()
                return true
            }R.id.btnRefresh -> {
            clearData()
        }

        }
        return super.onOptionsItemSelected(item)
    }
    fun clearData(){
        txtJumlah.setText("")
        txtKembalian.setText("Kembalian")
        txtDibayar.setText("")
        spCustomer.setSelection(0)
        spBrg.setSelection(0)
    }

    fun showDataCustomer(){
        val c : Cursor = db.rawQuery("select customerNm as _id from customer order by customerNm asc",null)
        spAdapter2 = SimpleCursorAdapter(this, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spCustomer.adapter = spAdapter2
        spCustomer.setSelection(0)
    }

    fun showDataBarang(){
        val c : Cursor = db.rawQuery("select barangNama as _id from barang order by barangNama asc",null)
        spAdapter = SimpleCursorAdapter(this, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spBrg.adapter = spAdapter
        spBrg.setSelection(0)
    }

    fun addTransaksi(){
        val number : Int = txtJumlah.text.toString().toInt()
        transaksiTotal = transaksiHrgBrg * number
        truangAnda = txtDibayar.text.toString().toInt()
        val uangKembali : Int = truangAnda - transaksiTotal
        txtKembalian.setText("Rp "+uangKembali)
       if(truangAnda > transaksiTotal){
           insertTr(brgId,brgId,number,brgHarga,transaksiTotal,uangKembali,getDate())
           Toast.makeText(this,"Berhasil menambahkan data transaksi", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(this,"Uang anda tidak cukup", Toast.LENGTH_SHORT).show()
        }
    }

    fun getDate(): String {
        var date =  SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(System.currentTimeMillis())
        return date
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun insertTr(barangId : String, customerId : String, jumlahBrg : Int, hargaBrg : String, totalByr : Int, kembalian : Int, tgl_tr : String){
        var cv : ContentValues = ContentValues()
        cv.put("brg_id", barangId)
        cv.put("customer_id",customerId)
        cv.put("jumlahBrg",jumlahBrg)
        cv.put("hargaBrg", hargaBrg)
        cv.put("totalByr",totalByr)
        cv.put("kembalian",kembalian)
        cv.put("tgl_tr",tgl_tr)
        db.insert("transaksi",null,cv)
    }


}