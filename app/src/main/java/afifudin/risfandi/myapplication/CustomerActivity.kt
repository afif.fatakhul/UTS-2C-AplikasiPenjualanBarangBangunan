package afifudin.risfandi.myapplication


import android.content.ContentValues
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_customer.*

class CustomerActivity :  AppCompatActivity(), View.OnClickListener  {
    lateinit var db : SQLiteDatabase
    lateinit var lsadapter: ListAdapter
    var cstmId : String = ""
    var idCstmr : String = ""
    var nmCstmr : String = ""
    var telpCstmr : String = ""
    var almtCstmr : String = ""
    val dtCstmr : Int = 100

    fun getDbObject() : SQLiteDatabase {
        db = DBHelper(this).writableDatabase
        return db
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnAddCustomer -> {
                var intent = Intent(this, AddCustomerActivity::class.java)
                intent.putExtra("isPage","Add")
                startActivityForResult(intent,dtCstmr)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer)
        btnAddCustomer.setOnClickListener(this)
        listCustomer.setOnItemClickListener(listClick)
        var paket : Bundle? = intent.extras
        cstmId = paket?.getString("customer_id").toString()
        var del = paket?.getString("isDeleted").toString()
        if(del.equals("yes")){
            var snackbar = Snackbar.make(LinearLayout, "1 customer telah dihapus", Snackbar.LENGTH_LONG)
                .setAction("Undo", undoDelete)
            snackbar.show()
        }
        getDbObject()
        val actionbar = supportActionBar
        actionbar!!.title = "Data Customer"
//        Back Button
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    fun undoDelCustomer(){
        var dtNull = "0000-00-00 00:00:00"
        var cv : ContentValues = ContentValues()
        cv.put("tgl_deleted",dtNull)
        db.update("customer",cv,"customerId = $cstmId",null)
    }

    val undoDelete = View.OnClickListener{
        undoDelCustomer()
        showDataCustomer()
        var snackbar = Snackbar.make(LinearLayout,"Customer berhasil di Undo", Snackbar.LENGTH_SHORT)
        snackbar.show()
    }

    fun showDataCustomer(){
        val c : Cursor = db.rawQuery("select customerId as _id,customerNm,customerTelp,customerAlmt,tgl_deleted from customer where tgl_deleted = '0000-00-00 00:00:00'  order by customerNm asc",null)
        lsadapter = SimpleCursorAdapter(this, R.layout.item_customer,c,
                arrayOf("customerNm","customerTelp","customerAlmt"), intArrayOf(R.id.txtNamaCstmr,R.id.txtTelp,R.id.txtALamat),
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        listCustomer.adapter = lsadapter
    }

    val listClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idCstmr = c.getString(c.getColumnIndex("_id"))
        nmCstmr = c.getString(c.getColumnIndex("customerNm"))
        telpCstmr = c.getString(c.getColumnIndex("customerTelp"))
        almtCstmr = c.getString(c.getColumnIndex("customerAlmt"))
        var intent = Intent( this,AddCustomerActivity::class.java)
        intent.putExtra("isPage","Edit")
        intent.putExtra("customer_id",idCstmr)
        intent.putExtra("customer_nama",nmCstmr)
        intent.putExtra("customer_telp",telpCstmr)
        intent.putExtra("customer_almt",almtCstmr)
        startActivityForResult(intent,dtCstmr)
    }

    override fun onStart() {
        super.onStart()
        showDataCustomer()
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}