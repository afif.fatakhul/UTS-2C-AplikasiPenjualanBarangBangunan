package afifudin.risfandi.myapplication

import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener{
    lateinit var db : SQLiteDatabase
    var txUsername : String=""
    val DATALOGIN : Int = 1


    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.btnLogin -> {
                if(proLogin(txtusername.text.toString(), txtpassword.text.toString())){
                    var intent = Intent(this, MainActivity::class.java)
                    intent.putExtra("getUsername", txUsername)
                    startActivityForResult(intent, DATALOGIN)
                }
                else{
                    Toast.makeText(this,"Periksa kembali username & password yang dimasukkan",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun getDbobject() : SQLiteDatabase{
        db = DBHelper(this).writableDatabase
        return db
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btnLogin.setOnClickListener(this)
        getDbobject()
    }

    fun proLogin(username: String, password: String): Boolean {
        var sql = "select username from user where username = '$username' AND password = '$password'"
        val c : Cursor = db.rawQuery(sql,null)
        if(c.count>0){
            c.moveToFirst()
            txUsername = c.getString(c.getColumnIndex("username"))
            return true
        }
        return false
    }

}