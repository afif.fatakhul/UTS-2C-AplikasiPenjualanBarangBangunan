package afifudin.risfandi.myapplication

import android.content.ContentValues
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_detailtransaksi.*
import java.text.SimpleDateFormat
import java.util.*

class DetailTransaksi: AppCompatActivity() {

    lateinit var db : SQLiteDatabase
    var trId : String = ""
    val dtTr : Int = 100

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_activity,menu)
        var paket : Bundle? = intent.extras
        menu?.findItem(R.id.btnDelete)?.setVisible(true)
        menu?.findItem(R.id.btnSimpan)?.setVisible(false)
        menu?.findItem(R.id.btnRefresh)?.setVisible(false)
        return super.onCreateOptionsMenu(menu)
    }

    fun getDbObject() : SQLiteDatabase {
        db = DBHelper(this).writableDatabase
        return db
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.btnDelete -> {
                deleteBarang()
                var intent = Intent(this, TransaksiActivity::class.java)
                intent.putExtra("isDeleted","yes")
                intent.putExtra("trId",trId)
                startActivityForResult(intent,dtTr)
            }


        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailtransaksi)
        getDbObject()
        var paket : Bundle? = intent.extras
        trId = paket?.getString("trId").toString()
        txtIdTr.setText("Id Transaksi\t: "+paket?.getString("trId"))
        txtNamaBrg.setText("Barang\t: "+paket?.getString("trBrg"))
        txtJumlahBrg.setText("Jumlah Barang\t: "+paket?.getString("jmlTr"))
        txtSatuanHrg.setText("Harga Barang per Item\t: Rp "+paket?.getString("trhrgBrg"))
        txtTotalHarga.setText("Total Transaksi\t: Rp "+paket?.getString("totalTr"))
        txtCustomer.setText("Customer\t: "+paket?.getString("customer"))
        txtTanggal.setText("Tanggal Transaksi\t: "+paket?.getString("tgl"))
        txtKembalian.setText("Kembalian\t: Rp "+paket?.getString("kembalianTr"))
        val actionbar = supportActionBar
        actionbar!!.title = "Detail Transaksi"
//        Back Button
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    fun getDate(): String {
        var date =  SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(System.currentTimeMillis())
        return date
    }

    fun deleteBarang(){
        var cv : ContentValues = ContentValues()
        cv.put("tgl_deleted",getDate())
        db.update("transaksi",cv,"trId = $trId",null)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}